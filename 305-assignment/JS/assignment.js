var userExists = false;
var newUserId = 10001;
var numofuser =0;
  
function processFormData() 
{
  var username_data = document.getElementById("username");
  var fname_data = document.getElementById("fname");
  var lname_data = document.getElementById("lname");
  var password_data =document.getElementById("password");
  var rpassword_data = document.getElementById("rpassword");
  var email_data = document.getElementById("email");
  var age_data = document.getElementById("age");
  
  var username = checkString(username_data.value);
  var fname = checkString(fname_data.value);
  var lname = checkString(lname_data.value);
  var password = checkString(password_data.value);
  var rpassword = checkString(rpassword_data.value);
  var email = checkString(email_data.value);
  var age = age_data.value;
  
  var error_message = 'The following fields had some errors : \n\n';
  var content = 'You inputed the following information: \n\n';
  var error_flag = false;
 
 

 if(username == '') {
	  error_message += 'User Name: Please enter your user name\n';
	  error_flag = true;
  } else {
	  content += 'User Name: ' + username + '\n';
  }


    
  
  
if(fname == '') {
	  error_message += 'First Name: Please enter your first name\n';
	  error_flag = true;
  } else {
	  content += 'First Name: ' + fname + '\n';
  }
  
  if(lname == '') {
	  error_message += 'Last Name: Please enter your Last name\n';
	  error_flag = true;
  } else {
	  content += 'Last Name: ' + lname + '\n';
  }
   
   if(password=='') {
      error_message += 'Password: Please enter your password\n';
	  error_flag = true; 
       
   } else if (rpassword==''){
      error_message += 'Confirm Password: Please enter your  password again\n';
	  error_flag = true; 
   } else if(!checkPassword(password, rpassword)) {
    error_message += 'Password: The value of password and Comfirm password should be same\n';
	  error_flag = true;
   }else {
	  content += 'Password : ' + password + '\n';
  }
  
  if(!checkEmail(email)) {
	  error_message += 'Email: Please enter a valid email address\n';
	  error_flag = true;
  } else {
	  content += 'Email : ' + email + '\n';
  }
  
   if(age == '') {
	  error_message += 'Age : Please enter your age\n';
	  error_flag = true;
  } else {
	  content += 'Age : ' + age + '\n';
  }
 checkExistUser(username, email);
  
  
  if (userExists) {
      error_message += 'User'+ username +'exists.\n';
	  error_flag = true;
 }else{
 }
 
      
 if(error_flag) {
	  alert(error_message);
  } else {
      
      
 checkData(username, fname, lname, email, password, age);
refreshData();
 }}

function checkData(username, fname, lname, email, password, age) {
    checkExistUser(username, email);
    generateUserID();
    console.log(newUserId);
    setTimeout(function() {
        updateToDb(username, fname, lname,email, password, age);
    }, 1500);
}



function updateToDb(username, fname, lname, email, password, age) {
    if (!userExists) {
    
        var userid = newUserId;
        
        var ndb = createDB("users", username);
        var userObj = {
          userID: userid,
  username: username,
  firstname: fname,
  lastname: lname,
  password: password,
  emailaddress:email,
  userage: age, };
  ndb.set(userObj);
  
  updateUserId(newUserId);
   refreshData();
   
   alert("User account registered sucessfully!")
   
}else{ alert("User Name / Email has been used! User account could not be registered.");
refreshData();
    
}}

function updateUserId(userid) {
    var db = createDB("userIdRef", null);
    db.set(userid);
}
  
function refreshData(){
    document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
    var db = createDB("users", null);
    db.on("child_added", function(snapshot) {
    var childData = snapshot.val();
  document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
})

 document.getElementById("text").innerHTML +="</table>";
}


function createDB(first, second) {
    var db = new Firebase("https://305assignment.firebaseio.com/");
    if (first == null)
        db = new Firebase("https://305assignment.firebaseio.com/");
    else if (second == null)
        db = new Firebase("https://305assignment.firebaseio.com/" + first + "/");
    else if (second != null)
        db = new Firebase("https://305assignment.firebaseio.com/" + first + "/" + second + "/");
    return db;
}

function generateUserID() {
    getUserId();
    setTimeout(function() {
        setNewUserId();
    }, 1500);
}

function getUserId() {
    var db = createDB("userIdRef", null);
    db.on("value", function(snapshot) {
        var value = snapshot.val();
        
        console.log("VALUE: " + value);
        
        
        if (value == null) {
            newUserId = 10000;
        }
        else {
            newUserId = parseInt(value);
            console.log("abc VALUE: " + value);
            
        }
    });
}

function setNewUserId() {
    newUserId += 1;
    
}


function checkExistUser(uname, email) {
    userExists = false;
    var ndb = createDB("users", null);
    ndb.on("child_added", function(snapshot) {
         var childData = snapshot.val();
         console.log("User Name: " + childData.username.toUpperCase());
         if(childData.username.toUpperCase()==uname.toUpperCase() || childData.Email == email) {
        userExists = true;
            console.log("LOOP exist: " + userExists);
         }
    });
    console.log("RETURN exist: " + userExists);
            
}


function checkString(str) {
  return str.replace(/^\s+|\s+$/g, '');
}



     

function checkEmail(email)
{	
	
  var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
  
  if(pattern.test(email)) {         
	return true;
  } else {   
	return false; 
  }

}

function checkPassword(password, rpassword){
    if (password == rpassword){
        return true;
    } else {   
	return false; 
  }
}

function SortbyName() {
 document.getElementById("abc").innerHTML = "User Info Sorted by Last Name.";
 document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
var ndb = createDB("users", null);
ndb.orderByChild("lastname").on("child_added", function(snapshot) {
    var childData = snapshot.val();
  document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
});}

function countAverage() {
    
    var total = 0;
  var numofuser =0;
     var wndb = createDB("users", null);
        wndb.on("value", function(snapshot) {
  snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
        total +=   Number(childData.userage);
        numofuser +=1;
   });
          alert("The Number of Registered Users:"+ numofuser +" \nThe Total age of Registered Users: " + total+" \nThe Average of Registered Users: " + total / numofuser);   
        });
      
       

}

function reset(){
    document.getElementById("username").innerHTML = null;
   document.getElementById("fname").innerHTML =null;
  document.getElementById("lname").innerHTML =null;
  document.getElementById("password").innerHTML =null;
  document.getElementById("rpassword").innerHTML =null;
  document.getElementById("email").innerHTML =null;
  document.getElementById("age").innerHTML =null;
}




function SortbyAge() {
    document.getElementById("abc").innerHTML ="User Info Sorted by Age.";
document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
var  ndb = createDB("users", null);
   ndb.orderByChild("userage").on("child_added", function(snapshot) {
    var childData = snapshot.val();
  document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
});}

function SortbyUserID() {
    document.getElementById("abc").innerHTML ="User Info Sorted by User ID.";
document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
var  ndb = createDB("users", null);
   ndb.orderByChild("userID").on("child_added", function(snapshot) {
    var childData = snapshot.val();
  document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
});}

function SortbyUserName() {
    document.getElementById("abc").innerHTML ="User Info Sorted by User Name.";
document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
var  ndb = createDB("users", null);
   ndb.orderByChild("username").on("child_added", function(snapshot) {
    var childData = snapshot.val();
  document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
});}



function agefilter(){
   
    var min = document.getElementById("ageA").value;
    var max = document.getElementById("ageB").value;
    console.log(min,max);
    console.log(validFilterAge(min,max));
    if (!validFilterAge(min, max)) {
        
        alert("Invalid filter value must be 10-90, please re-enter.");
          refreshData(); 
    }else {
        console.log(validFilterAge(min, max));
     document.getElementById("abc").innerHTML ="Showing User age from "+ min+" to "+ max+".";
    document.getElementById("text").innerHTML =" <tr><td>User ID</td><td>User Name</td><td>First Name</td><td>Last name</td><td>Email Address</td><td>User Age</td></tr>";
  var ref = createDB("users",null)
   ref.orderByChild("userage").startAt(min).endAt(max).on("child_added", function(snapshot) {
        var childData = snapshot.val();
document.getElementById("text").innerHTML += "<tr><td>"+childData.userID+"</td>"
    +"<td>"+childData.username+"</td>"
    +"<td>"+childData.firstname+"</td>"
    +"<td>"+childData.lastname+"</td>"
    +"<td>"+childData.emailaddress+"</td>"
    +"<td>"+childData.userage+"</td></tr>";
        
    });
    }
}


function validFilterAge(min, max) {
    var valid = true;
    var a = parseInt(min);
    var b = parseInt(max);
     if(min== " " || max == "" ){
          
     valid = false;
    }else if(a <=10 || a>90){
       
        valid = false;
        
    }else if(b <=10 || b>90){
         
        valid = false;
       
    }if(a>b){
        
        valid = false;
        
    }
    return valid;
}